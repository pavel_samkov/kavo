//Pavlo Samkov 
#include <iostream>
#include <string>
typedef unsigned int uint;
using namespace std;




class List{
    struct Node{
        int next_position;
        string word;
        Node* next;
        Node* prev;

    };

    Node* start = nullptr;
    Node* pointer = nullptr;

    int position;
    int counter_nodes = 0;
    uint counter_zero = 0;

    int trigger; //zmienna pomocnicza wskazujaca ze stoimy na nodzie
    int i = 0; //licznik dla dodatkowej listy
public:
    List(){}
    List(int start);
    void push_back(string word, int value);
    void push_prev(Node* kek);
    void erase_node(Node* tmp);

    Node* get_node(int val);
    string solver(List *l, int* tab_y);
    uint get_count_zero(){return counter_zero;}
    Node* get_node_from_sec_list(List *l);
};
List::List(int start)
{
    position = start;
    trigger = 1; //na poczatku zawsze zaczynamy od zerowej(1) nody
}

void List::push_back(string word, int value)
{
    Node* tmp = new Node;
    tmp->word = word;
    tmp->next_position = value;

    if(value == 0)
        counter_zero++;

    if(start == nullptr)
    {
        start = tmp;
        start->next = start;
        start->prev = start;
        pointer = start;
    }
    else
    {
        Node* last = start->prev;
        tmp->next = start; // komunikacja nowego last i start
        start->prev = tmp; // komunikacja nowego last i start
        tmp->prev = last; // komunikacja nowego last i starego last
        last->next = tmp; // komunikacja nowego last i starego last
    }
    counter_nodes++;
}

void List::push_prev(Node* kek)
{

    pointer->prev->next = kek;
    kek->prev = pointer->prev;
    kek->next = pointer;
    pointer->prev = kek;


    trigger = 1; //o ile zostajemy na miejscu
    counter_nodes++;
}

string List::solver(List *l, int* tab_y)
{
    string res;
    while(counter_nodes > 0)
    {
        pointer = get_node(position); //wyciagamy kolejny nod
        position = pointer->next_position;
        if(position == 0)
        {
            Node* kek = l->get_node_from_sec_list(l);
            push_prev(kek);
            position = tab_y[i++];//wyciagamy liczbe z drugiej listy
            pointer->next_position = position;//zapisujemy zamiast zera liczbe
            continue;
        }
        res += pointer->word + " ";
        erase_node(pointer);
    }
    return res;
}

List::Node* List::get_node_from_sec_list(List *l)
{
    Node* tmp = l->start;
    l->start=l->start->next;
    return tmp;
}


List::Node* List::get_node(int val)
{
    int c = abs(val);
    if(val == 0)
        return pointer;
    if(counter_nodes == 1)
        return pointer->next;


     if(c % counter_nodes == 0 && trigger)
          {
              return pointer;
          }
     else if(c % counter_nodes == 0)
          {
          if(val > 0)
              pointer = pointer->prev;
          else
              pointer = pointer->next;
          }




        c = c % counter_nodes;
        if(counter_nodes - c < c)
        {
            if(trigger == 1)
                c = counter_nodes - c;
            else
                c = counter_nodes - c + 1;
            if(val > 0)
                while(c > 0)
                {
                    pointer = pointer->prev;
                    c--;
                }
            else
            {
                while(c > 0)
                {
                    pointer = pointer->next;
                    c--;
                }
            }

        }
        else
        {
            if(val > 0)
                while(c > 0)
                {
                    pointer = pointer->next;
                    c--;
                }
            else
            {
                while(c > 0)
                {
                    pointer = pointer->prev;
                    c--;
                }
            }
        }

    return pointer;
}

void List::erase_node(Node* to_del)
{
    if (to_del == start)
    {
        start->prev->next = start->next;
        start->next->prev = start->prev;
        start = start->next;
    }
    else
    {
        to_del->prev->next = to_del->next;
        to_del->next->prev = to_del->prev;

    }
    trigger = 0; //bo wiemy ze stoimy na nie na nodzie
    counter_nodes--;
}
int main()
{
    uint n,p;
    string word;
    int x,start;

    cin >> n >> start;
    List l(start);

    for(uint i = 0; i < n; i++)
    {
        cin >> word >> x;
        l.push_back(word, x);
    }


    cin >> p;
    p = l.get_count_zero();
    List l_2;

    int* tmp_values = new int[p];
    for(uint j = 0; j < p; j++)
    {
        cin >> word >> x >> tmp_values[j];
        if(tmp_values[j] == 0 && x == 0)
        {
            p=p+2;
            tmp_values = new int[p];
           // realloc(tmp_values, p);
        }
        else if(tmp_values[j] == 0 || x == 0){
            p++;
            tmp_values = new int[p];
           // realloc(tmp_values, p);
        }
        l_2.push_back(word,x);
    }
    cout<<l.solver(&l_2, tmp_values)<<endl;
    return 0;
}
