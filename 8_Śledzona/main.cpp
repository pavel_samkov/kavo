//Pavlo Samkov 
#include <iostream>
#include <string>

using namespace std;

std::string test(unsigned &x, unsigned &y, unsigned &id);
pair<unsigned int, unsigned int> binary_search(unsigned int i, unsigned int l_x, unsigned int r_x, unsigned int l_y, unsigned int r_y)
{
    unsigned int mid_x = l_x + (r_x - l_x)/ 2;
    unsigned int mid_y = l_y + (r_y - l_y)/ 2;

    if(test(mid_x, mid_y, i) == "")
        return make_pair(mid_x, mid_y);
    if(test(mid_x, mid_y, i) == "N")
        return binary_search(i, mid_x, mid_x, mid_y + 1, r_y);
    if(test(mid_x, mid_y, i) == "NE")
        return binary_search(i, mid_x + 1, r_x, mid_y + 1, r_y);
    if(test(mid_x, mid_y, i) == "NW")
        return binary_search(i, l_x, mid_x - 1, mid_y + 1, r_y);
    if(test(mid_x, mid_y, i) == "S")
        return binary_search(i, mid_x, mid_x, l_y, mid_y - 1);
    if(test(mid_x, mid_y, i) == "SW")
        return binary_search(i, l_x, mid_x - 1, l_y, mid_y - 1);
    if(test(mid_x, mid_y, i) == "SE")
        return binary_search(i, mid_x + 1, r_x, l_y, mid_y - 1);
    if(test(mid_x, mid_y, i) == "W")
        return binary_search(i, l_x, mid_x - 1, mid_y, mid_y);
    if(test(mid_x, mid_y, i) == "E")
        return binary_search(i, mid_x + 1, r_x, mid_y, mid_y);




}
int main()
{
    ios_base::sync_with_stdio(false);
    unsigned int t, x, y;
    cin >> x >> y >> t;

    pair<unsigned int, unsigned int> res;

    for(unsigned int i = 0; i < t; i++)
    {
        res = binary_search(i, 0, x, 0 ,y);
        cout <<res.first <<" "<< res.second<<endl;
    }



    return 0;
}
std::string test(unsigned &x, unsigned &y, unsigned &id) {
    unsigned xTest, yTest;
    std::string direction = "";
    switch( id ) {
    case 0:
        xTest = 5;
        yTest = 5;
        break;
    case 1:
        xTest = 14;
        yTest = 16;
        break;
    default:
        xTest = 0;
        yTest = 0;
        break;
    }
    if(yTest < y)
        direction = "S";
    else if(yTest > y)
        direction = "N";
    if(xTest < x)
        direction += "W";
    else if(xTest > x)
        direction += "E";
    return direction;
}
