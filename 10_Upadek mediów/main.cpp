//Pavlo Samkov 
#include <iostream>
#include <vector>
using namespace std;

struct input_set{
    int f_p,f_d,s_p,s_d;
};


int main()
{
    int n,s,d,l,m;
    int res = 1;
    input_set p;
    int sum = 0;

    vector<input_set>* arr = new vector<input_set>[7];

    cin >> n >> s >> d >> l >> m;
    int** array = new int*[n];

    for(int i = 0; i < n; i++)
        array[i] = new  int[7];

    for (int i = 0;i < n;i++)
        for (int j = 0;j < 7;j++)
            array[i][j] = 0;

    for(int i = 0; i < m; i++)
    {
      cin >> p.f_p >> p.f_d >> p.s_p >> p.s_d;
      switch(p.f_d)
      {
        case 0:arr[0].push_back(p); break;
        case 1:arr[1].push_back(p); break;
        case 2:arr[2].push_back(p); break;
        case 3:arr[3].push_back(p); break;
        case 4:arr[4].push_back(p); break;
        case 5:arr[5].push_back(p); break;
        case 6:arr[6].push_back(p); break;
        default: return 1;
      }
    }

    array[s][d] = 1;

    for (int j = d + 1;j < d + l;j++)
    {

       for(auto i : arr[j%7])
       {
        array[i.f_p][i.f_d] += array[i.s_p][i.s_d]%100000007;

        res += array[i.s_p][i.s_d];
        res = res%100000007;
       }


      for (int i = 0;i < n;i++)
       {
        if(j%7 == 6)
          array[i][0] = 0;
        else
          array[i][(j%7)+1] = 0;
       }

    }

    cout << res%100000007 <<endl;
    return 0;
}
