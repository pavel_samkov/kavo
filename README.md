# Algorithms 

Rozwiazania zadan z kursu "**Algorytmy i Struktury Danych I - 2019/2020**" - UMCS


* 2_Werbowanie - implementacja algorytmu 'Selection Sort'
* 3_Spisek - implementacja struktury 'List' i dzialania na niej
* 4_Eksperyment - implementacja struktury 'Queue' i dzialania na niej
* 5_Drzewo dowodów - implementacja struktury 'Tree' i dzialania na niej 
* 6_Informator - implementacja algorytmu 'BFS' 
* 7_Kiepska asystentka - implementacja algorytmu 'Topological sort' 
* 8_Śledzona - implementacja algorytmu 'Binary Search' 
* 9_Analiza eksperta - Problem wyboru zajęć za pomoca 'Greedy Algorithm'
* 10_Upadek mediów - Problem plecakowy za pomoca metody 'Dynamic Programming'
* 11_Wolontariusze - Problem pokrycia wierzchołkowego za pomoca metody 'Backtracking'
* 12_Porządki - implementacja algorytmow 'Count Sort', 'Radix Sort', 'Bucket Sort' 
* 13_Rządy - implementacja struktury 'Heap' i dzialania na niej


