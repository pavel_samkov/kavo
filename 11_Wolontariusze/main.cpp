//Pavlo Samkov 
#include <iostream>
#include <stack>
using namespace std;

struct krawedz
{
    int a,b,c = 0;
    bool check_a = false;
    bool check_b = false;
};
bool checker(krawedz* arr, int m)
{
    for (int i = 0;i < m;i++)
    {
        if(arr[i].c)
            continue;
        else
            return false;
    }
    return true;
}
//funkcja dla odznaczania z ktorych krawedzi nie korzystamy
void decrease(krawedz* arr, int m, int what)
{
    for (int i = 0;i < m;i++)
    {
        if(arr[i].a == what || arr[i].b == what)
            arr[i].c--;
    }
}
//funkcja dla odznaczania ktore krawedzi sa wybrane
void increase(krawedz* arr, int m, int what)
{
    for (int i = 0;i < m;i++)
    {
        if(arr[i].a == what || arr[i].b == what)
            arr[i].c++;
    }
}

//tmp dla kontroli liczby analizowanych,last_element ostatnia liczba ze stosu
bool solver(krawedz* arr, int m, int r, int* stack, int tmp = 0,int last_element=0)
{
    int i = 0,check_number = 0;
    while(arr[i].c != 0)//szukamy pierwszego zera w liscie
        i++;

    if(!arr[i].check_a)
    {
        check_number = arr[i].a;
        arr[i].check_a = true;
    }
    else if(!arr[i].check_b)
    {
        check_number = arr[i].b;
        arr[i].check_b = true;
    }
    else
    {
        if(arr[0].check_a && arr[0].check_b)
            return false;
        //decrease(arr, m, last_element);
        arr[tmp].check_a = true;
        arr[tmp].check_b = true;
        decrease(arr, m, stack[tmp]);
        tmp--;
        if(solver(arr,m,r,stack,tmp,last_element))
         return true;
    }

    tmp++;
    stack[tmp] = check_number;
    last_element = check_number;

    increase(arr, m, check_number);
    if(checker(arr,m) && tmp<=r)
        return true;
    if(tmp == r)
    {
        decrease(arr, m, check_number);
        stack[tmp] = 0;
        tmp--;
    }
    //cout<<"na stosie"<<check_number<<endl;

    if(solver(arr,m,r,stack,tmp,last_element))
        return true;

}

int main()
{
    int t, n, m, r;
    //stack <int>* s = new stack<int>;
    krawedz* arr;
    cin >> t;
    string* results = new string[t];
    for (int i = 0; i < t; i++)
    {
       cin >> n >> m >> r;
       int* stack = new int[r+1];
       arr = new krawedz[m];
       for (int j = 0;j < m; j++)
       {
           cin >> arr[j].a >> arr[j].b;
       }
       if(solver(arr, m, r,stack))
           results[i] = "TAK";
       else
           results[i] = "NIE";
    }
    for (int i = 0; i < t; i++)
    {
        cout<<results[i]<<endl;
    }
    return 0;
}
