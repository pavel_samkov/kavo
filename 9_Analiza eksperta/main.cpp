//Pavlo Samkov 
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
typedef unsigned int uint;
struct pas
{
    uint a,b;
};

bool compare(pas a, pas b)
{
    return a.b < b.b;
}

int solver(vector<pas> a,  int n)
{
    int res = 1, last = 0;
    sort(a.begin(), a.end(),compare);
        for (int j = 0; j < n; j++)
        {
            if(a[last].b <= a[j].a)
            {
                res++;
                last = j;
            }
            else
            {
                continue;
            }
        }
    return res;
}
int main()
{
    ios_base::sync_with_stdio(false);

    int t,n;
    vector<pas> a;
    pas kek;

    cin >> t;
    for (int i = 0;i < t;i++)
    {
        cin >> n;
        for (int j = 0;j < n;j++)
        {
            cin >> kek.a >> kek.b;
            a.push_back(kek);
        }
        cout << solver(a,n)<< endl;
        a.clear();
    }
    return 0;
}
