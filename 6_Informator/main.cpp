//Pavlo Samkov 
#include <iostream>
#include <queue>
//up,down,left,right (0, 1, 2, 3)//dir aby miec dostep po indexas w BFS
using namespace std;

struct Node{
bool visited = false;
char value;
int m = 0;
Node* dir[4];
};

void BFS(Node* start)
{
    int nodes_next_layer = 0;//w nastepnym obszarze
    int nodes_left_layer = 1;//czyli ile V zostalo w danym (obszarze) dlugosci n
    int curr_lenght = 0;

    queue<Node*> q;
    Node* tmp = new Node;
    start->visited = true;
    q.push(start);

    while(!q.empty())
    {
        tmp = q.front();
        if(tmp->value == 'X')
        {
            cout<<curr_lenght<<" "<<tmp->m<<endl;
            return;
        }
        q.pop();
        for(int i = 0; i < 4; i++)
        {
            if(tmp->dir[i])
            {
                if(!tmp->dir[i]->visited)
                {
                    if(tmp->dir[i]->value == 'M')
                        tmp->dir[i]->m = tmp->m + 1;
                    else
                        tmp->dir[i]->m = tmp->m;

                    tmp->dir[i]->visited = true;//w tym rozwiazaniu ustawiam odwiedzonych jak tylko wrzucam do kolejki
                    q.push(tmp->dir[i]);
                    nodes_next_layer++;
                }
                else if(tmp->m < tmp->dir[i]->m)//jak napotykamy odwiedzonego sasiada i on ma wiecej drog monitorowanych, to wybieramy droge z mniej odwiedzonymi M
                {
                    if(tmp->dir[i]->value == 'M')//jezeli to M to wiadomo
                        tmp->dir[i]->m = tmp->m + 1;
                    else
                        tmp->dir[i]->m = tmp->m;
                }
            }
            else
                continue;


        }
        nodes_left_layer--;
        if(nodes_left_layer == 0)//szukamy tu nakrotshej sciezki, czyli jak skonczylismy obszar(sciezke dlugosci n) to robimy ++
        {
            nodes_left_layer = nodes_next_layer;
            nodes_next_layer = 0;
            curr_lenght++;
        }
    }

}

void drive(int m,int n)
{
    Node* start = new Node;//pointer na start
    Node*** tmp = new Node**[m];
    for (int i = 0;i < m;i++)
        tmp[i] = new Node*[n];

    for (int i = 0;i < m;i++)
    {
        for (int j = 0;j < n;j++)
        {
            tmp[i][j] = new Node;
            cin>>tmp[i][j]->value;
            if(tmp[i][j]->value == '#')//jezeli wczytamy hash to continue, jezeli nie to znaczy ze wczytalismy poprawny vierzcholek(. M S X)
            {
                tmp[i][j] = nullptr;
                continue;
            }
            else
            {
                if(tmp[i][j]->value == 'S')//ustawiamy start
                {
                    *start = *tmp[i][j];
                    tmp[i][j] = start;
                }

                if(i == 0)//znaczy ze jak jestesmy w pierwszym wiershu to nie mamy sasiadow na gorze
                    tmp[i][j]->dir[0] = nullptr;
                else
                    if(tmp[i-1][j])//sprawdzamy czy gorny element nie #, jak nie to ustawiamy relacje pomiedzy nimi
                    {
                        tmp[i][j]->dir[0] = tmp[i-1][j];
                        tmp[i-1][j]->dir[1] = tmp[i][j];
                    }
                    else
                        tmp[i][j]->dir[0] = nullptr;//jak hash to nullptr

                if(j == 0)//znaczy ze jak jestesmy w pierwszej columnie to nie mamy sasiadow z lewa
                    tmp[i][j]->dir[2] = nullptr;
                else
                    if(tmp[i][j-1])//czy element z lewa nie #, jak nie to ustawiamy relacje pomiedzy nimi
                    {
                        tmp[i][j]->dir[2] = tmp[i][j-1];
                        tmp[i][j-1]->dir[3] = tmp[i][j];
                    }
                    else
                        tmp[i][j]->dir[2] = nullptr;

                if(j == n-1)//w ostatniej kolumnie nie mamy prawego sasiada
                   tmp[i][j]->dir[3] = nullptr;

                if(i == m-1)//w osatnim wierszu nie mamy dolnego sasiada
                   tmp[i][j]->dir[1] = nullptr;
            }
        }
    }
    BFS(start);
}
int main()
{
    int n,m; //vartosci poczatkowe
    cin >> m >>n;
    drive(m,n);
    return 0;
}
