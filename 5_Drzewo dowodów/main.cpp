//Pavlo Samkov 
#include<iostream>
using namespace std;

struct Node
{
    int value;
    Node* parent = nullptr;
    Node* l_child = nullptr;
    Node* r_brother = nullptr;
};

class Tree
{

public:
    Node* root;
    Node* poisk;
    Tree();
    Node* create_node(int val);
    void add(int* values, int n);
    void add_child(Node* to, Node* who);
    void add_brother(Node* to, Node* who);
    bool check_node_child(Node *kek);
    Node* search(int n);
    void visit(Node* root);
};
Tree::Tree()
{
    root = new Node;
    root->l_child = nullptr;
    root->r_brother = nullptr;
    root->parent = nullptr;
    root->value = 0;
}
Node* Tree::create_node(int val)
{
    Node* tmp = new Node;
    tmp->value = val;
    tmp->r_brother = nullptr;
    tmp->l_child = nullptr;
    return tmp;
}
void Tree::add_child(Node* k_komu, Node* kavo)
{
    kavo->parent = k_komu;
    k_komu->l_child = kavo;
    kavo->r_brother = nullptr;
}
bool Tree::check_node_child(Node* kek)
{
    return kek->l_child;
}
void Tree::add_brother(Node* k_komu, Node* kavo)
{
    Node* tmp2 = new Node;
    tmp2 = k_komu->l_child;

    while(tmp2->r_brother)
        tmp2 = tmp2->r_brother;

    tmp2->r_brother = kavo;
    kavo->parent = k_komu;
    kavo->r_brother = nullptr;
}
Node* Tree::search(int n)
{
    while(poisk->value != n || poisk->r_brother != nullptr)
    {
        poisk = poisk->r_brother;
    }
    if(poisk->value == n)
        return poisk;
    else {

    }
    return poisk;
}
void Tree::add(int *values, int n)
{
    Node* tmp;
    Node* tmp2 = root;
    Node* tmp3;
    for (int i = 0; i < n; i++)
    {
        if(tmp2->l_child)
        {
            tmp3 = tmp2->l_child;
            while(tmp3->r_brother && tmp3->value != values[i])
            {
                tmp3 = tmp3->r_brother;
            }
            if(tmp3->value == values[i])
            {
                tmp2 = tmp3;
                continue;
            }
            else
            {
                tmp = create_node(values[i]);
                add_brother(tmp2, tmp);
                tmp2 = tmp;
            }
        }
        else
        {
            tmp = create_node(values[i]);
            add_child(tmp2, tmp);
            tmp2 = tmp;
        }
    }
}

void Tree::visit(Node* root)
{
    if(root == nullptr)
        return;

    while(root)
    {

        cout<<root->value<<" ";
        if(root->l_child)
            visit(root->l_child);
        root = root->r_brother;
    }
}

int main(){
ios_base::sync_with_stdio(false);
Tree t;
int n,x;
int* y;
cin >> n;
for(int i = 0; i < n; i++)
{
    cin >> x;
    y = new int[x];
    for(int j = 0; j < x; j++)
    {
        cin >> y[j];
    }
    t.add(y, x);
    delete [] y;
}
t.visit(t.root->l_child);
return 0;
}



