//Pavlo Samkov 
#include<iostream>
#include<string>
#define div 1000003
// Use Selection sort


struct pers
{
    unsigned int x;
    std::string name;
};

void swap(pers *a, pers *b)
{
    pers c = *a;
    *a = *b;
    *b = c;
}

void sorter(unsigned int n, pers *tab)
{
   unsigned int i, j, imin;
       for(i = 0; i<n-1; i++)
       {
          imin = i;
          for(j = i+1; j<n; j++)
          {
             if(tab[j].x < tab[imin].x)
             {
                imin = j;
             }
             else if(tab[j].x == tab[imin].x) //jezeli napotkamy taki sam elment odrazu sortujemy alfabetycznie
             {
                 if(tab[j].name > tab[imin].name)
                     imin = j;
             }
          }
             swap(&tab[i], &tab[imin]);
       }
}

long int solver(unsigned int n, pers *tab)
{
    long int res = 1;
    for(unsigned int i = 1;i<n;i++)
        res += 1 + i * tab[n-i-1].x;

    res %= div;
    return res;
}

std::string solver_2(unsigned int n,unsigned int p,  pers *tab)
{
   unsigned int i;
   std::string res="";
   for(i = 1;i <= p;i++)
       res += tab[n-i].name + " ";

   return res;
}



int main()
{
   std::ios_base::sync_with_stdio(false);

   unsigned int t,n=0,p=0;
   std::cin >> t;

   std::string* res_2 = new std::string[t];
   long  int *res = new long[t];
   pers **wsk = new pers*[t];


   for(unsigned int j = 0; j < t; j++)
   {
       std::cin >> n >> p;
       wsk[j] = new pers[n];
       for(unsigned int i = 0; i < n; i++)
            std::cin >> wsk[j][i].name >> wsk[j][i].x;

       sorter(n, wsk[j]);

       res_2[j] = solver_2(n,p,wsk[j]);
       res[j] = solver(n, wsk[j]);
   }

   for(unsigned int j = 0; j < t; j++)
   {
       std::cout<<res_2[j]<<std::endl;
       std::cout<<res[j]<<std::endl;
   }
   return 0;
}

