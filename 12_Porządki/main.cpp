//Pavlo Samkov 
#include<iostream>
#include <algorithm>
#include <vector>
using namespace std;

struct document
{
	int unique_number;
	int priority;
	long random_number;
	int topics[4];
};
bool compare(document a, document b)
{
	return a.random_number < b.random_number;
}

void count_sort_unique(document* kek, int n)
{
	int* pom = new int[n + 1];
	document* sort = new document[n + 1];

	for(int i = 0; i < n + 1; i++)
		pom[i] = 0;

	for(int i = 0; i < n; i++)
		pom[kek[i].unique_number]++;
		
	pom[0]--;
	for(int i = 1; i < n + 1; i++)
		pom[i] += pom[i - 1];

	for(int i = n - 1; i >= 0; i--)
	{
		sort[pom[kek[i].unique_number]] = kek[i];
		pom[kek[i].unique_number]--;
	}
	for(int i = 0; i < n; i++)
	{
		kek[i] = sort[i];
	}
	delete [] sort;
	delete [] pom;
}

void count_sort_priority(document* kek, int n)
{
	int* pom = new int[11];
	document* sort = new document[n + 1];

	for(int i = 0; i < 11; i++)
		pom[i] = 0;

	for(int i = 0; i < n; i++)
		pom[kek[i].priority]++;
		
	pom[0]--;
	for(int i = 1; i < 11; i++)
		pom[i] += pom[i - 1];

	for(int i = n - 1; i >= 0; i--)
	{
		sort[pom[kek[i].priority]] = kek[i];
		pom[kek[i].priority]--;
	}
	for(int i = 0; i < n; i++)
	{
		kek[i] = sort[i];
	}
	delete [] sort;
	delete [] pom;
}

void radix_sort(document* kek, int n)
{
	document* sort = new document[n + 1];

	for(int j = 3;j >= 0;j--)
	{
		int* pom = new int[257];

		for(int i = 0; i < 257; i++)
			pom[i] = 0;

		for(int i = 0; i < n; i++)
			kek[i].topics[j]++;

		for(int i = 0; i < n; i++)
			pom[kek[i].topics[j]]++;
			
		pom[0]--;
		for(int i = 1; i < 257; i++)
			pom[i] += pom[i - 1];

		for(int i = n - 1; i >= 0; i--)
		{
			sort[pom[kek[i].topics[j]]] = kek[i];
			pom[kek[i].topics[j]]--;
		}
		for(int i = 0; i < n; i++)
			sort[i].topics[j]--;

		for(int i = 0; i < n; i++)
			kek[i] = sort[i];
	}

	delete [] sort;
}
void bucket_sort(document* kek, int n)
{
	vector<document>* pom = new vector<document>[n];

	for(int i = 0; i < n; i++)
	{
		int index = 101 * kek[i].random_number / 100000;
		pom[index].push_back(kek[i]);
	}
	int id = 0;
	for(int i = 0; i < n; i++)
	{
		sort(pom[i].begin(), pom[i].end(),compare);
		for(int j = 0; j < pom[i].size();j++)
		{
			kek[id] = pom[i][j];
			id++;
		}
	}
	for(int i = 0; i < n; i++)
		pom[i].clear();
	delete [] pom;
}
int solver(document* kek, int* order, int n)
{
	int res = 0;

	for(int i = 3; i >= 0; i--)
	{
		switch(order[i])
		{
			case 1: count_sort_unique(kek, n); break;
			case 2: count_sort_priority(kek, n); break;
			case 3:	bucket_sort(kek, n); break;
			case 4: radix_sort(kek, n); break;
			default: return 1;
		}
	}
	for(int i = 0; i < n; i++)
	{
		res += (kek[i].random_number * i)%100000007;
		res %= 100000007;
	}
	return res;
}

int main()
{
    std::ios_base::sync_with_stdio(false);

	int n,tmp1;
	int* order = new int[4];

	cin >> n;
	document* kek = new document[n];

	for(int i = 0; i < n;i++)
	{
		cin >> kek[i].unique_number >> kek[i].priority >> kek[i].random_number >> tmp1;
		for(int j = 0; j < 4; j++)
			kek[i].topics[j] = -1;

		for(int j = 0; j < tmp1; j++)
			cin >> kek[i].topics[j];
	}

	for(int i = 0; i < 4; i++)
	{
		cin >> order[i];
	}


	cout << solver(kek, order, n)%100000007 <<endl;
	delete [] kek;
	delete [] order;
	return 0;
}