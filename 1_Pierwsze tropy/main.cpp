//Pavlo Samkov 
#include<iostream>
#include<cmath>
int solver(int n, int max, int max_r)
{
    if(n - max <= 0) //badamy czy w pierszym kroku mozemy skonczyc
        return 1;
    else if(max_r == 0) //zwracamy 0 jezeli max_roznica bedzie = 0, np dla przypadku jednej metody
        return 0;
    int kek = static_cast<int>(ceil((static_cast<double>(n - max)/max_r) + 1.0));
    return kek;
}


int main()
{
    std::ios_base::sync_with_stdio(false);
    int t,m;
    int n;
    std::cin >> t;
    int* res= new int[t];
    int max_x = 0;
    int max_roznica = 0;
    int x, y;
    for(int j = 0; j < t; j++)
    {
        std::cin >> n >> m;
        max_x = 0;
        max_roznica = 0;
        for(int i = 0; i < m; i++)
        {
            std::cin >> x >> y;
            if(max_x < x)
                max_x = x;
            if(max_roznica < x - y)
                max_roznica = x - y;//nawet jezeli roznica bedzie ujemn, ustawi max_roznica na 0
        }
        res[j] = solver(n, max_x,max_roznica);
    }
    for(int j = 0; j < t; j++)
    {
        if(res[j] <= 0)
            std::cout<<"BRAK METOD"<<std::endl;
        else
            std::cout<<res[j]<<std::endl;
    }
        return 0;
}
