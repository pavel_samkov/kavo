//Pavlo Samkov 
#include <iostream>
#include <string>
typedef unsigned int uint;
using namespace std;

struct Person
{

    string f_name;
    string s_name;
    uint power;
    Person* next;
    Person* prev;
};

class Queue
{
    Person* start = nullptr;
    int sum = 0;
    uint max = 0 , min = 0;

public:
    Queue();
    void push(string f_name, string s_name, uint power);
    void push(Person *a);
    Person* pop(uint n);
    void show(uint n);

    uint get_min(uint n);
    uint get_max(uint n);
    int get_sum(){return sum;}
    uint get_diff_in_squad();
};

Queue::Queue()
{}


void Queue::push(string f_name, string s_name, uint power)
{
    Person* tmp = new Person;
    tmp->f_name = f_name;
    tmp->s_name = s_name;
    tmp->power = power;

    if(start == nullptr)
    {
        start = tmp;
        start->next = start;
        start->prev = start;
        max = min = power;
    }
    else
    {
        Person* last = start->prev;
        tmp->next = start;
        start->prev = tmp;
        tmp->prev = last;
        last->next = tmp;
    }

    if(power > max)
        max = power;
    else if(min > power)
        min = power;

    sum += power;
}

void Queue::push(Person *a)
{
    if(start == nullptr)
    {
        start = a;
        start->next = start;
        start->prev = start;
    }
    else
    {
        Person* last = start->prev;
        a->next = start;
        start->prev = a;
        a->prev = last;
        last->next = a;
    }
    if(a->power > max)
        max = a->power;
    else if(min > a->power)
        min = a->power;

    sum += a->power;
}
Person* Queue::pop(uint n)
{
    Person* tmp = start;

    start->prev->next = start->next;
    start->next->prev = start->prev;
    start = start->next;
    sum -= tmp->power;

    if(tmp->power == max)
        max = get_max(n);
    else if(tmp->power == min)
        min = get_min(n);

    return tmp;
}

void Queue::show(uint n)
{
    Person* tmp = start;
    uint counter = n/2;
    while(counter > 0)
    {
        //tmp = pop();
        if(counter == n/2)
            cout<<endl;
        cout<<tmp->f_name<<" "<<tmp->s_name<<endl;
        tmp = tmp->next;
        counter--;
    }
}
uint Queue::get_max(uint n)
{
    Person* tmp = start;
    uint max = tmp->power;

    for(uint c = 0; c < n/2 ; c++)
    {
        tmp = tmp->next;
        if(max < tmp->power)
            max = tmp->power;
    }
    return max;
}

uint Queue::get_min(uint n)
{

    Person* tmp = start;
    uint min = tmp->power;
    for(uint c = 0; c < n/2; c++)
    {
        tmp = tmp->next;
        if(min > tmp->power)
            min = tmp->power;
    }
    return min;
}
uint Queue::get_diff_in_squad()
{
    return max - min;
}


void solver(Queue *a,Queue *b, uint v, uint n)
{
    bool trigger = false;


    uint diff_1 = a->get_diff_in_squad(); //funkcja do roznicy w zespole
    uint diff_2 = b->get_diff_in_squad();//funkcja do roznicy w zespole
    int av_1 = 0;//funkcja do sriedniej w zespole
    int av_2 = 0;//funkcja do sriedniej w zespole
    int diff_average = 0; // roznica sriednich
    int min_diff = -1;


    uint licznik = 0;
    if(n == 2)
    {
        a->show(n);
        b->show(n);
        return ;
    }

    for(uint c = 0; c != n/2; c++)
    {
        if(diff_1 <= v && diff_2 <= v)
        {
            trigger = true;
            av_1 = a->get_sum();
            av_2 = b->get_sum();
            diff_average = abs(av_1 - av_2);

            if(min_diff > diff_average || min_diff == -1)
            {
                min_diff = diff_average;
                licznik = c;
                if(min_diff == 0)
                    break;
            } 


        }
        b->push(a->pop(n));
        a->push(b->pop(n));
        diff_1 = a->get_diff_in_squad();
        diff_2 = b->get_diff_in_squad();

    }

    if(!trigger)
        cout<<"NIE"<<endl;
    else
    {
        if(min_diff == 0)
        {
            a->show(n);
            b->show(n);
        }
        else {
            for(uint i = 0; i < licznik; i++)
            {
                b->push(a->pop(n));
                a->push(b->pop(n));
            }
            b->show(n);
            a->show(n);
        }


    }



}

int main()
{
    ios_base::sync_with_stdio(false);
    uint n = 0,v; //maks roznica
    string f_name,l_name;

    Person ts;
    Queue q,p;
    cin >> n >> v;
    for(uint j = 0; j < n/2; j++)
    {
        cin >> ts.f_name >> ts.s_name >> ts.power;
        q.push(ts.f_name, ts.s_name, ts.power);
    }
    for(uint j = 0; j < n/2; j++)
    {
        cin >> ts.f_name >> ts.s_name >> ts.power;
        p.push(ts.f_name, ts.s_name, ts.power);
    }
    solver(&q, &p, v, n);

    return 0;
}
