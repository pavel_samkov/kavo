//Pavlo Samkov 
#include<iostream>
#include<map>
#include<vector>
using namespace std;

struct Node{
    string l_name;
    int input = 0;
    vector<Node*> adj;
};

int main(){
    std::ios_base::sync_with_stdio(false);

    Node* tmp;
    bool check = true;
    string string_1,string_2;
    int t, n, m;

    cin>>t;
    for(int i = 0; i < t; i++)
    {
        cin >> n >> m;
        map<string, Node*> kek;
        for(int j = 0;j < n;j++)
        {
            tmp = new Node;
            cin>>tmp->l_name;
            kek.insert(pair<string, Node*>(tmp->l_name, tmp));
        }
        for(int j = 0;j < m;j++)
        {
            cin >> string_1 >> string_2;
            kek.find(string_1)->second->adj.push_back(kek.find(string_2)->second);
            kek.find(string_2)->second->input++;
        }
        for(int j = 0;j < n;j++)
        {
            cin >> string_1;
            if(check)//znaczy ze juz mamy bledny wynik i poprostu wczytujemy liste
            {
                tmp = kek.find(string_1)->second;
                if(tmp->input == 0)
                {
                    for(auto it : tmp->adj)
                        it->input--;
                }
                else
                {
                    check = false;
                }
            }
        }
        if(check)
            cout<<"TAK"<<endl;
        else
            cout<<"NIE"<<endl;
        check = true;
    }
    return 0;
}
